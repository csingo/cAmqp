package cAmqp

import (
	"fmt"
	"time"

	"github.com/rabbitmq/amqp091-go"
)

func load() {
	// 读取connection配置
	if len(amqp_config.Connections) > 0 {
		for name, item := range amqp_config.Connections {
			uri := fmt.Sprintf(
				"amqp://%s:%s@%s:%d/%s",
				item.Username,
				item.Password,
				item.Host,
				item.Port,
				item.VHost,
			)
			// self.saveConnection(name, uri, amqp091.Config{
			//	Heartbeat: time.Duration(item.Heartbeat) * time.Second,
			// }, ConnectionType_Consumer)
			// self.saveConnection(name, uri, amqp091.Config{
			//	Heartbeat: time.Duration(item.Heartbeat) * time.Second,
			// }, ConnectionType_Producer)
			self.saveConnection(name, uri, amqp091.Config{
				Heartbeat: time.Duration(item.Heartbeat) * time.Second,
			})
		}
	}

	// 读取driver配置
	if len(amqp_config.Drivers) > 0 {
		for name, item := range amqp_config.Drivers {
			self.saveDriver(name, item)
		}
	}
}
