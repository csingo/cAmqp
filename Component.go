package cAmqp

import (
	"gitee.com/csingo/cComponents"
)

type AmqpComponent struct{}

func (i *AmqpComponent) Inject(instance any) bool {
	return false
}

func (i *AmqpComponent) InjectConf(config cComponents.ConfigInterface) bool {
	if config.ConfigName() == AmqpConfigName {
		amqp_config = config.(*AmqpConf)
		return true
	}
	return false
}

func (i *AmqpComponent) Load() {
	load()
}

func (i *AmqpComponent) Listen() []*cComponents.ConfigListener {
	return []*cComponents.ConfigListener{}
}

var Component = &AmqpComponent{}
