push:
	# 更新包
	go get -u
	go mod tidy
	goimports -local gitee.com/csingo -w .
	# 更新版本
	version/creator `cat version/version` > version/version
	# 提交代码
	git add .
	git commit -m "update"
	git push
	# 提交标签
	git tag -af `cat version/version` -m ""
	git push --tags
