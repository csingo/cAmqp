package cAmqp

import (
	"github.com/gin-gonic/gin"
	"github.com/rabbitmq/amqp091-go"

	"gitee.com/csingo/cLog"
)

func Confirm(ctx *gin.Context, data amqp091.Delivery, ack AckType) {
	var err error
	switch ack {
	case ACK:
		err = data.Ack(false)
	case NACK:
		err = data.Nack(false, false)
	case REQUEUE:
		err = data.Reject(true)
	case DROP:
		err = data.Reject(false)
	default:
		err = AckTypeErr
	}
	if err != nil {
		cLog.WithContext(ctx, map[string]any{
			"source":   "cAmqp.Confirm",
			"body":     string(data.Body),
			"exchange": data.Exchange,
			"route":    data.RoutingKey,
			"err":      err.Error(),
		}).Error("AMQP 消费者ack异常")
	}
}
