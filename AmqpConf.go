package cAmqp

const (
	AmqpConfigName = "AmqpConf"
)

type ExchangeTypeName string

const (
	EXCHANGE_TYPE_DIRECT  ExchangeTypeName = "direct"
	EXCHANGE_TYPE_FANOUT  ExchangeTypeName = "fanout"
	EXCHANGE_TYPE_TOPIC   ExchangeTypeName = "topic"
	EXCHANGE_TYPE_HEADERS ExchangeTypeName = "headers"
)

type AmqpConf struct {
	Connections map[string]*AmqpConf_Connection `json:"connections"`
	Drivers     map[string]*AmqpConf_Driver     `json:"drivers"`
}

type AmqpConf_Connection struct {
	Host      string `json:"host"`
	Port      int64  `json:"port"`
	Username  string `json:"username"`
	Password  string `json:"password"`
	VHost     string `json:"vhost"`
	Heartbeat int64  `json:"heartbeat"`
}

type AmqpConf_Driver struct {
	Connection   string           `json:"connection"`
	ExchangeName string           `json:"exchange_name"`
	ExchangeType ExchangeTypeName `json:"exchange_type"`
	QueueName    string           `json:"queue_name"`
	RoutingKey   string           `json:"routing_key"`
}

func (i *AmqpConf) ConfigName() string {
	return AmqpConfigName
}

var amqp_config *AmqpConf
