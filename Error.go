package cAmqp

type AmqpError string

const (
	AmqpConfDriverNotFoundErr     AmqpError = "AmqpConf driver 配置不存在"
	AmqpConfDriverNameNotFoundErr AmqpError = "AmqpConf driver name 不存在"
	AmqpConfConnectionNotFoundErr AmqpError = "AmqpConf connection 配置不存在"
	AmqpChannelCloseErr           AmqpError = "amqp channel 已关闭"

	AckTypeErr AmqpError = "ack 类型错误"
)

func (e AmqpError) Error() string {
	return string(e)
}
